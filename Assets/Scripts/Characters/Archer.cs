﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Archer : CharacterBase 
{
    ICharacterVisitor<StatsDistribution> _statsDistributionArcher = new StatsDistributionVisitor();
    public override int Level { get; protected set; }
    public override CharacterStats Stats { get; protected set; }
    public override void Attack(IDamageable other)
    {
        other.ReceiveDamage(this.CalculateDamage());
    }

    public override int CalculateDamage()
    {

        var stats = _statsDistributionArcher as StatsDistributionVisitor;
        
        if (stats == null) return 1;
        
        var distribution = stats.Visit(this);
        
        var agilityPart = this.Stats.Agility * distribution.AgilityPercentage;
        var intelligencePart = this.Stats.Intelligence * distribution.IntelligencePercentage;
        var strengthPart = this.Stats.Strength * distribution.StrengthPercentage;
        
        return Mathf.FloorToInt(agilityPart + intelligencePart + strengthPart);
    }

    public override void IncreaseLevel()
    {
        this.Level++;

        CharacterStats deltas;
    }
}
