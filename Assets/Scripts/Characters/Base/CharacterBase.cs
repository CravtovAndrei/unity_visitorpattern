﻿using UnityEngine;

public abstract class CharacterBase : MonoBehaviour
{
    public abstract int Level { get; protected set; }
    public abstract CharacterStats Stats { get; protected set; }
    public abstract void Attack(IDamageable other);
    public abstract int CalculateDamage();
    public abstract void IncreaseLevel();
}
