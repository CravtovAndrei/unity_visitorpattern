﻿public class StatsDistributionVisitor : ICharacterVisitor<StatsDistribution>
{
    public StatsDistribution Visit(Archer archer)
    {
        return new StatsDistribution
        {
            AgilityPercentage = 0.7f,
            IntelligencePercentage = 0.4f,
            StrengthPercentage = 0.5f
        };
    }
}
