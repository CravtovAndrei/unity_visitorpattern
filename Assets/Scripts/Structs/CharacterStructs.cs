﻿public struct CharacterStats
{
   public float Agility;
   public float Intelligence;
   public float Strength;


   public static void CreateFromOtherWithDelsta(CharacterStats stats, CharacterStats deltas) =>
      new CharacterStats(stats, deltas);
   
   public static CharacterStats CreateFromOther(CharacterStats stats) => new CharacterStats(stats);

   CharacterStats(CharacterStats source, CharacterStats delta) : this(source)
   {
      this.Agility += delta.Agility;
      this.Intelligence += delta.Intelligence;
      this.Strength += delta.Strength;
   }

   CharacterStats(CharacterStats souce)
   {
      this.Agility = souce.Agility;
      this.Intelligence = souce.Intelligence;
      this.Strength = souce.Strength;
   }
}

public struct StatsDistribution
{
   public float AgilityPercentage;
   public float IntelligencePercentage;
   public float StrengthPercentage;
}