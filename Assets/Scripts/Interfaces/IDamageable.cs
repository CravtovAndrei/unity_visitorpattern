﻿public interface IDamageable
{
    void ReceiveDamage(int calculateDamage);
}
